# Airflow Attribution Pipeline Report

## Introduction
The Airflow Attribution Pipeline is a comprehensive data processing and reporting system designed to attribute conversions to various marketing channels. This report provides an overview of the pipeline's structure, the role of its individual tasks, and the flow of data from extraction to report generation.

## Pipeline Structure
The Attribution Pipeline is organized as a Directed Acyclic Graph (DAG) within Apache Airflow. It consists of a sequence of tasks that ensure the efficient processing of data and the generation of attribution reports. The core tasks within the pipeline include:

* Extract Data: This task queries data from a database within a specified time range.

* Transform Data: Extracted data is transformed into a format suitable for analysis and reporting.

* Load Data: The transformed data is sent to an Integrated Health Care (IHC) API and loaded into a database.

* Merge Data: This task fills the "channel_reporting" table in the database.

* Generate Report: The final attribution report is generated based on the merged data.

## Configuration
The pipeline is highly configurable and relies on parameters stored in a YAML configuration file (config.yaml). Key configuration parameters include:

* DB_PATH: The path to the database.
* IHC_API_URL: The URL for the IHC API.
* API_KEY: An API key for authentication.
* CONV_TYPE_ID: The conversion type identifier.
* Time ranges for data extraction.


## Task Dependencies
The pipeline's tasks are defined with clear dependencies to ensure orderly execution:

* Extract Data depends on the database path (DB_PATH) and time range for extraction.
* Transform Data depends on the output of Extract Data.
* Load Data relies on both the transformed data and the IHC API configuration.
* Merge Data is executed upon successful completion of Load Data and depends on the DB_PATH.
* Generate Report depends on the successful completion of Merge Data and relies on the DB_PATH.

## DAG Execution
The pipeline's Directed Acyclic Graph (DAG) is executed under the following settings:

* Owner: "rakibulmdalam"
* Start Date: October 13, 2023
* Schedule Interval: None (Manual triggering, not scheduled at regular intervals).
* Catchup: False (Tasks are not backfilled for previous dates).


## Data Flow
Data flows logically through the pipeline:

* Data is extracted from the database within the specified time range.
* Extracted data is transformed into a format suitable for analysis and reporting.
* The transformed data is loaded into the IHC API and written to the database.
* Data from the API and the database is merged into the "channel_reporting" table.
* The final attribution report is generated based on the merged data.


## Conclusion
The Airflow Attribution Pipeline is an essential tool for processing and attributing conversions to various marketing channels. By using Apache Airflow, it ensures robust and reliable data processing. For further improvements, consider adding error handling, logging, and monitoring to enhance the pipeline's reliability in production environments and to provide comprehensive data analysis for marketing decision-making.
