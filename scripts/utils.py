import json
import sqlite3
import requests
import yaml
import pandas as pd


def load_config(config_file):
    with open(config_file, "r") as file:
        config_data = yaml.safe_load(file)
    return config_data


# Define a Python function to fetch all conv_ids from the SQLite database
def fetch_conv_ids_from_db(db_path):
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    cursor.execute("SELECT DISTINCT conv_id FROM conversions;")
    conv_ids = [row[0] for row in cursor.fetchall()]
    conn.close()
    return conv_ids


def query_sessions_and_conversions(db_path, time_range=None):
    try:
        # Establish a connection to the SQLite database
        conn = sqlite3.connect(db_path)
        cursor = conn.cursor()

        # Write the SQL query to filter data by the time range if provided
        sql_query = """
        SELECT
            c.conv_id,
            s.session_id,
            s.user_id,
            s.event_date,
            s.event_time,
            s.channel_name,
            s.holder_engagement,
            s.closer_engagement,
            s.impression_interaction
        FROM
            conversions c
        JOIN
            session_sources s
        ON
            c.user_id = s.user_id
            AND date(c.conv_date) >= date(s.event_date)
        """

        if time_range:
            sql_query += " WHERE DATE(s.event_date) BETWEEN DATE(:start_date) AND DATE(:end_date);"
            query_params = {
                "start_date": time_range[0],  # Start date of the time range
                "end_date": time_range[1],  # End date of the time range
            }

            cursor.execute(sql_query, query_params)
        else:
            cursor.execute(sql_query)

        # Fetch the data as a list of dictionaries
        column_names = [desc[0] for desc in cursor.description]
        data = [dict(zip(column_names, row)) for row in cursor.fetchall()]

        # Commit the transaction and close the connection
        conn.commit()
        conn.close()

        # write data to csv using pandas
        filename = "airflow_pipelines/data/extracted_data.csv"
        df = pd.DataFrame(data)
        df.to_csv(filename, index=False)

        return filename
    except Exception as e:
        raise e


def create_customer_journeys_csv(input_filename):
    # read extracted_data.csv and transform those data into customer_journeys.csv
    df = pd.read_csv(input_filename)
    df["timestamp"] = df["event_date"] + " " + df["event_time"]
    df = df.drop(["event_date", "event_time"], axis=1)

    # rename conv_id to conversion_id
    df = df.rename(columns={"conv_id": "conversion_id"})
    # rename channel_name to channel_label
    df = df.rename(columns={"channel_name": "channel_label"})
    # set conversion to 0
    df["conversion"] = 0

    # remove columns except the following
    # session_id, conversion_id, channel_label, timestamp,
    # holder_engagement, closer_engagement, impression_interaction,
    # conversion
    df = df[
        [
            "session_id",
            "conversion_id",
            "channel_label",
            "timestamp",
            "holder_engagement",
            "closer_engagement",
            "impression_interaction",
            "conversion",
        ]
    ]

    # write to csv
    output_filename = "airflow_pipelines/data/all_customer_journeys.csv"
    df.to_csv(output_filename, index=False)

    return output_filename


def call_api(customer_journeys, full_api_url, api_key):
    body = {
        "customer_journeys": customer_journeys,
    }

    response = requests.post(
        full_api_url,
        data=json.dumps(body),
        headers={"Content-Type": "application/json", "x-api-key": api_key},
    )

    # Check if the request was successful
    if response.status_code == 200:
        attribution_results = response.json()["value"]
        return attribution_results
    else:
        print(response.json())
        print("API request failed with status code: %s", response.status_code)
        response.raise_for_status()


def write_attribution_results(attribution_results, db_path):
    try:
        conn = sqlite3.connect(db_path)
        cursor = conn.cursor()

        # Insert ihc results into the attribution_customer_journey table
        for result in attribution_results:
            conv_id = result["conversion_id"]
            session_id = result["session_id"]
            ihc = result["ihc"]
            cursor.execute(
                """
                INSERT INTO attribution_customer_journey (conv_id, session_id, ihc)
                VALUES (?, ?, ?);
                """,
                (conv_id, session_id, ihc),
            )

        # Commit changes and close the database connection
        conn.commit()
        conn.close()
        return True
    except Exception as e:
        raise e


def write_channel_reporting_data(db_path):
    try:
        # Establish a connection to the SQLite database
        conn = sqlite3.connect(db_path)
        cursor = conn.cursor()

        # Write SQL query to fill the channel_reporting table
        sql_query = """
            INSERT INTO channel_reporting (channel_name, date, cost, ihc, ihc_revenue)
            SELECT
                s.channel_name,
                s.event_date,
                SUM(COALESCE(sc.cost, 0)),            -- Use COALESCE to handle NULL values
                SUM(COALESCE(acj.ihc, 0)),            -- Use COALESCE to handle NULL values
                SUM(COALESCE(acj.ihc * c.revenue, 0)) -- Use COALESCE to handle NULL values
            FROM session_sources s
            LEFT JOIN session_costs sc ON s.session_id = sc.session_id
            LEFT JOIN attribution_customer_journey acj ON s.session_id = acj.session_id
            LEFT JOIN conversions c ON acj.conv_id = c.conv_id
            GROUP BY s.channel_name, s.event_date;
        """

        # Execute the SQL query
        cursor.execute(sql_query)

        # Commit the transaction and close the connection
        conn.commit()
        conn.close()
        return True
    except Exception as e:
        raise e


def get_channel_data(db_path):
    # Establish a connection to the SQLite database
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()

    # Write the SQL query to select data from the channel_reporting table
    sql_query = """
    SELECT
        cr.channel_name,
        cr.date,
        cr.cost,
        cr.ihc,
        cr.ihc_revenue,
        -- Calculate CPO (cost per order)
        CASE WHEN cr.ihc > 0 THEN cr.cost / cr.ihc ELSE 0 END AS CPO,
        -- Calculate ROAS (return on ad spend)
        CASE WHEN cr.cost > 0 THEN cr.ihc_revenue / cr.cost ELSE 0 END AS ROAS
    FROM
        channel_reporting cr;
    """

    # Execute the SQL query
    cursor.execute(sql_query)

    # Fetch the data as a list of dictionaries
    column_names = [desc[0] for desc in cursor.description]
    data = [dict(zip(column_names, row)) for row in cursor.fetchall()]

    return data
