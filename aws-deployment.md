# Apache Airflow Deployment with AWS Elastic Beanstalk and Terraform

This repository contains the necessary files and instructions for deploying your Apache Airflow pipeline to AWS Elastic Beanstalk using Terraform.

## Prerequisites

Before you begin, make sure you have the following prerequisites:

- [Terraform](https://www.terraform.io/) installed on your local machine.
- [AWS CLI](https://aws.amazon.com/cli/) installed and configured with your AWS access and secret keys.

## Getting Started

1. Clone this repository to your local machine.

2. Navigate to the `terraform` directory:

   ```bash
   cd terraform

3. Initialize the Terraform project:

   ```bash
   terraform init

4. Review and edit the main.tf file in the terraform directory to customize your deployment.

Update the region in the provider "aws" block to your desired AWS region.

Modify the application and environment names in the aws_elastic_beanstalk_application and aws_elastic_beanstalk_environment resources.

When using a separate RDS for airflow, configure the AIRFLOW__CORE__SQL_ALCHEMY_CONN environment variable to specify your database connection.

Replace the service role name in the setting block for "aws:elasticbeanstalk:environment" if you have a custom service role.

5. Validate the configuration:

   ```bash
   terraform plan

6. Apply the terraform configuration to create the elasticbeanstalk environment

    ```bash
    terraform apply

7. Package Your Apache Airflow Code:

Before using the AWS CLI, make sure you've packaged your Apache Airflow code and dependencies into a ZIP file. Replace your_airflow_directory with the actual path to your Apache Airflow code directory:

    ```
    zip -r airflow_code.zip ./

8. Deploy the code

    ```bash
    # Retrieve values from Terraform variables
    aws_region="eu-central-1"  # Set the AWS region
    application_name="airflow-application"  # Set the Elastic Beanstalk application name
    version_label="version-label"  # Set the desired version label
    s3_bucket_name=<"s3-bucket-name">  # Set the name of S3 bucket
    zip_file_name=<"airflow_code.zip">  # Set the name of ZIP file

    # Use AWS CLI to create an application version
    aws elasticbeanstalk create-application-version --application-name "$application_name" \
    --version-label "$version_label" --source-bundle "S3Bucket=$s3_bucket_name,S3Key=$zip_file_name"
