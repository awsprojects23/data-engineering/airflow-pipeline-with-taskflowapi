from datetime import datetime
from airflow import DAG

from airflow_pipelines.tasks.tasks import (
    extract_data,
    transform_data,
    load_data,
    merge_data,
    generate_report,
)

from airflow_pipelines.scripts.utils import load_config

default_args = {
    "owner": "rakibulmdalam",
    "start_date": datetime(2023, 10, 13),
}


# using the TaskFlow API
with DAG(
    "attribution_taskflow",
    default_args=default_args,
    description="Attribution Pipeline DAG",
    schedule_interval=None,
    start_date=datetime(2023, 10, 13),
    catchup=False,
) as dag:
    # Load configuration from the YAML file
    config_data = load_config("airflow_pipelines/config/config.yaml")
    DB_PATH = config_data["db_path"]
    IHC_API_URL = config_data["ihc_api_url"]
    API_KEY = config_data["api_key"]
    CONV_TYPE_ID = config_data["conv_type_id"]

    # input data for the taskflow
    time_range = (config_data["start_time_range"], config_data["end_time_range"])
    report_csv = "channel_reporting.csv"

    # Define task dependencies through function calls
    extracted_data_filename = extract_data(DB_PATH, time_range)
    transformed_data_filename = transform_data(extracted_data_filename)
    load_success = load_data(
        IHC_API_URL, API_KEY, CONV_TYPE_ID, transformed_data_filename, DB_PATH
    )
    merge_success = merge_data(load_success, DB_PATH)
    generate_report(merge_success, DB_PATH, report_csv)
