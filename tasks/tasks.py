import pandas as pd
from airflow.decorators import task

from scripts.utils import (
    call_api,
    create_customer_journeys_csv,
    get_channel_data,
    query_sessions_and_conversions,
    write_attribution_results,
    write_channel_reporting_data,
)


@task
def extract_data(db_path, time_range=None):
    print("Querying data from the database...")
    try:
        data_filename = query_sessions_and_conversions(db_path, time_range)
        return data_filename
    except Exception as e:
        print(
            f"Error querying data: {str(e)}",
        )
        raise e


@task
def transform_data(input_data):
    print("Transforming data...")

    try:
        if input_data:
            # Iterate over the list of dictionaries and transform each customer journey
            transformed_data_filename = create_customer_journeys_csv(input_data)
            return transformed_data_filename
        else:
            raise ValueError("No data to transform.")
    except Exception as e:
        print("Error during data transformation: %s", str(e))
        raise e


@task
def load_data(api_url, api_key, conv_type_id, input_csv, db_path):
    print("Sending data to the IHC API and loading into database...")

    # Read data from the CSV file
    transformed_data = pd.read_csv(input_csv)

    # filter data by conversion_id and send those to the API
    # get distinct conversion_id
    conversion_ids = transformed_data["conversion_id"].unique()
    # for each conv_id filter df and convert to json
    # and then send to the API
    # and then write the response to the database
    for conv_id in conversion_ids:
        # filter data by conversion_id
        data = transformed_data[transformed_data["conversion_id"] == conv_id]
        print(data.to_dict(orient="records"))
        # send data to the API
        try:
            full_api_url = f"{api_url}?conv_type_id={conv_type_id}"
            attribution_results = call_api(
                data.to_dict(orient="records"), full_api_url, api_key
            )
            write_attribution_results(attribution_results, db_path)
        except Exception as e:
            print(f"Error in response from the API:{str(e)}")

    return True


@task
def merge_data(load_done, db_path):
    if load_done:
        try:
            # Fill the channel_reporting table
            return write_channel_reporting_data(db_path)
        except Exception as e:
            print("Error filling channel_reporting table: %s", str(e))
    else:
        raise Exception("Error filling channel_reporting table. Check previous task.")

    return True


@task
def generate_report(merge_done, db_path, output_csv):
    if merge_done:
        try:
            # get channel data from the database and
            # save it to a csv file
            data = get_channel_data(db_path)
            report_df = pd.DataFrame(data)
            report_df.to_csv(output_csv, index=False)
        except Exception as e:
            print("Error generating CSV file: %s", str(e))
            raise
    else:
        raise ValueError("Error generating CSV file. Check previous task.")
