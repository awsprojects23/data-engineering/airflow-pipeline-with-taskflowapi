# Define the AWS provider
provider "aws" {
  region = var.aws_region
}

# Define input variables
variable "aws_region" {
  description = "The AWS region where resources will be created."
  type        = string
  default     = "eu-central-1" 
}

variable "application_name" {
  description = "Name of the Elastic Beanstalk application."
  type        = string
  default     = "airflow-application" 
}

variable "version_label" {
  description = "Label for the Elastic Beanstalk application version."
  type        = string
  default     = "version-label"
}

variable "s3_bucket_name" {
  description = "Name of the S3 bucket containing your ZIP file."
  type        = string
}

variable "zip_file_name" {
  description = "Name of the ZIP file containing your Apache Airflow code."
  type        = string
}

variable "environment_name" {
  description = "Name of the Elastic Beanstalk environment."
  type        = string
  default     = "airflow-environment-name" 
}

# Create an Elastic Beanstalk application
resource "aws_elastic_beanstalk_application" "airflow" {
  name = var.application_name
}

resource "aws_elastic_beanstalk_application_version" "default" {
  name        = "ebs-application-version-label"
  application = "ebs-airflow-application"
  description = "application version created by terraform"
  bucket      = aws_s3_bucket.default.id
  key         = aws_s3_object.default.id
}

# Create an Elastic Beanstalk environment
resource "aws_elastic_beanstalk_environment" "airflow" {
  name                = var.environment_name
  application         = aws_elastic_beanstalk_application.airflow.name
  solution_stack_name = "64bit Amazon Linux 2 v4.1.0 running Python 3.8"  # Change to the desired stack

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "AIRFLOW__CORE__SQL_ALCHEMY_CONN"
    value     = "sqlite:///airflow.db"  # Use SQLite for local database; update this based on your database setup
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "aws-elasticbeanstalk-service-role"  # Replace with your service role name
  }
}
